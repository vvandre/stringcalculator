﻿using Calculator;
using System;

namespace appStringCalculator {
    class Program {
        static void Main(string[] args) {
            bool bExit = false;
            StringCalculator sCalc = new StringCalculator();
            while (!bExit) {
                
                Console.WriteLine("[Type the Delimiter]");
                string sUserDelimiter = Console.ReadLine();

                Console.WriteLine("Type the numbers to be calculated separated by one of this [ " + sUserDelimiter + " ] delimiters");                
                string sUserInput = Console.ReadLine();
                sUserDelimiter = "//" + sUserDelimiter;

                try {
                    int iResult = sCalc.Add(sUserDelimiter + Environment.NewLine + sUserInput);
                    Console.WriteLine(string.Format("Result: {0}", iResult));
                }                
                catch (Exception ex) {
                    Console.WriteLine(ex.Message.ToString());
                }

                Console.WriteLine("[Enter] to a new operation or type 'exit' to quit!");
                sUserInput = Console.ReadLine();
                bExit = sUserInput.ToUpper() == "Exit".ToUpper() ? true : false;
                Console.WriteLine();
                Console.WriteLine();
            }
            sCalc = null;
        }        
    }    
}