﻿using System;

namespace Calculator
{
    public class StringCalculator {
        public int Add(string numbers) {
            int iReturn = 0;
            string sNegativeNumbers = "";
            string sDelimiterDefault = ",";

            if (!string.IsNullOrEmpty(numbers)) {
                string[] sGeneralValues = numbers.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                string[] sDelimiters = sGeneralValues.Length == 2 ? getDelimite(sGeneralValues[0]) : new string[] { };
                string[] sNumbers = getStringNumbersPrepared(sDelimiters, sDelimiterDefault, sGeneralValues[1]);                
                foreach (string sNumber in sNumbers) {
                    int _number = 0;
                    bool isNumber = int.TryParse(sNumber, out _number);
                    if (_number < 0 || _number > 1000) {
                        sNegativeNumbers = sNegativeNumbers + (_number < 0 ? "[" + _number.ToString() + "]" : "");
                        _number = 0;
                    }
                    iReturn += _number;
                }                
            }

            if (!string.IsNullOrEmpty(sNegativeNumbers)) {
                System.ArgumentOutOfRangeException negativeEx = new System.ArgumentOutOfRangeException("Negatives not allowed " + sNegativeNumbers);
                throw negativeEx;
            }

            return iReturn;
        }

        private string[] getDelimite(string delimiters) {
            return delimiters.Replace("/", "").Split(',');
        }

        private string[] getStringNumbersPrepared(string[] delimiters, string delimiterDefault, string numbers) {
            string[] sResult = null;
            string _numbers = numbers;
            foreach (string sDelimiter in delimiters) {
                _numbers = _numbers.Replace(sDelimiter, delimiterDefault);
            }

            if (_numbers.IndexOf(delimiterDefault) > 0) {
                sResult = _numbers.Split(delimiterDefault.ToCharArray());
            }
            else {                
                sResult = new string[] { _numbers };
            }
            return sResult;
        }
    }
}
