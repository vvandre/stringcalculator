﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Calculator.Tests
{
    [TestClass()]
    public class SringCalculatorTests
    {
        [TestMethod()]
        public void testAdd_EmptyValue()
        {
            int isExpected = 0;
            StringCalculator oCalc = new StringCalculator();
            int iResult = oCalc.Add("");
            Assert.AreEqual(isExpected, iResult);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Negatives not allowed.")]
        public void testAdd_NegativeValues()
        {
            StringCalculator oCalc = new StringCalculator();
            int iResult = oCalc.Add("//@" + Environment.NewLine + "1@-1@-3");
            Assert.Fail();
        }

        [TestMethod()]
        public void testAdd_Result8()
        {
            int isExpected = 8;
            StringCalculator oCalc = new StringCalculator();
            int iResult = oCalc.Add("//@" + Environment.NewLine + "1@2@5");
            Assert.AreEqual(isExpected, iResult);
        }

        [TestMethod()]
        public void testAdd_MultiplesDelimiter()
        {
            int isExpected = 25;
            StringCalculator oCalc = new StringCalculator();
            int iResult = oCalc.Add("//@,#" + Environment.NewLine + "1@2@5#4@4#9");
            Assert.AreEqual(isExpected, iResult);
        }

        [TestMethod()]
        public void testAdd_1001()
        {
            int isExpected = 25;
            StringCalculator oCalc = new StringCalculator();
            int iResult = oCalc.Add("//@,#" + Environment.NewLine + "1@2@5#4@4#9@1001");
            Assert.AreEqual(isExpected, iResult);
        }

        [TestMethod()]
        public void testAdd_ArbitraryLen() {
            int isExpected = 25;
            StringCalculator oCalc = new StringCalculator();
            int iResult = oCalc.Add("//@@@@@@@,#" + Environment.NewLine + "1@@@@@@@2@@@@@@@5#4@@@@@@@4#9@@@@@@@1001");
            Assert.AreEqual(isExpected, iResult);
        }
    }
}